/*
  Copyright (c) 2019 CommonsWare, LLC

  Licensed under the Apache License, Version 2.0 (the "License"); you may not
  use this file except in compliance with the License. You may obtain	a copy
  of the License at http://www.apache.org/licenses/LICENSE-2.0. Unless required
  by applicable law or agreed to in writing, software distributed under the
  License is distributed on an "AS IS" BASIS,	WITHOUT	WARRANTIES OR CONDITIONS
  OF ANY KIND, either express or implied. See the License for the specific
  language governing permissions and limitations under the License.

  Covered in detail in the book _Elements of Kotlin Coroutines_

  https://commonsware.com/Coroutines
*/

package com.commonsware.coroutines.weather

import android.content.Context
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.subscribeBy
import org.threeten.bp.OffsetDateTime
import org.threeten.bp.ZoneId
import org.threeten.bp.format.DateTimeFormatter

data class RowState(
  val timestamp: String,
  val icon: String,
  val temp: String?,
  val wind: String?,
  val pressure: String?
) {
  companion object {
    fun fromModel(
      model: ObservationModel,
      formatter: DateTimeFormatter,
      context: Context
    ): RowState {
      val timestampDateTime = OffsetDateTime.parse(
        model.timestamp,
        DateTimeFormatter.ISO_OFFSET_DATE_TIME
      )
      val easternTimeId = ZoneId.of("America/New_York")
      val formattedTimestamp =
        formatter.format(timestampDateTime.atZoneSameInstant(easternTimeId))

      return RowState(
        timestamp = formattedTimestamp,
        icon = model.icon,
        temp = model.temperatureCelsius?.let {
          context.getString(
            R.string.temp,
            it
          )
        },
        wind = model.windSpeedMetersSecond?.let { speed ->
          model.windDirectionDegrees?.let {
            context.getString(
              R.string.wind,
              speed,
              it
            )
          }
        },
        pressure = model.barometricPressurePascals?.let {
          context.getString(
            R.string.pressure,
            it
          )
        }
      )
    }
  }
}

sealed class MainViewState {
  object Loading : MainViewState()
  data class Content(val observations: List<RowState>) : MainViewState()
  data class Error(val throwable: Throwable) : MainViewState()
}

class MainMotor(
  private val repo: IObservationRepository,
  private val formatter: DateTimeFormatter,
  private val context: Context
) : ViewModel() {
  private val _states =
    MutableLiveData<MainViewState>().apply { value = MainViewState.Loading }
  val states: LiveData<MainViewState> = _states
  private val sub = CompositeDisposable()

  init {
    sub.add(
      repo.load()
        .observeOn(AndroidSchedulers.mainThread())
        .subscribeBy(onNext = { models ->
          _states.value = MainViewState.Content(models.map {
            RowState.fromModel(it, formatter, context)
          })
        }, onError = { _states.value = MainViewState.Error(it) })
    )
  }

  override fun onCleared() {
    sub.dispose()
  }

  fun refresh() {
    sub.add(repo.refresh()
      .observeOn(AndroidSchedulers.mainThread())
      .subscribeBy(onError = { _states.value = MainViewState.Error(it) })
    )
  }

  fun clear() {
    sub.add(repo.clear()
      .observeOn(AndroidSchedulers.mainThread())
      .subscribeBy(onError = { _states.value = MainViewState.Error(it) })
    )
  }
}